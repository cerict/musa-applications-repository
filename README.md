# MUSA Chef Repository - Application

The application repository hosts the cookbooks related to the application to be started throught MUSA.

# Repository structure

The Chef Repository contains a set of directories where all the cookbooks are described. 
Each cookbook must have its own directory and the name of the directory represents the name of the cookbook. The cookbook must be define in accordance with [Chef cookbooks guidelines](https://docs.chef.io/cookbook_repo.html)

## Default cookbook structure

* musa-applications-repository/
  * aaaaas/
  * dbaas/
  * tut/