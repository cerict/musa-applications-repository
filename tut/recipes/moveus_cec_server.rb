#
# Cookbook Name:: sla-platform
# Recipe:: sla-manager
#
# Copyright 2010-2015, Cerict, Napoli
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#     ----------------------------------------
#
# This product includes software developed at "Company", http://url/ .
#
# Developers:
#  * De Rosa Pasquale <pakygta@gmail.com>
#

include_recipe "enabling-platform::jdk_v8"

bash "folder_creation" do
    user "root"
    cwd  "/opt"
    code <<-EOH
    mkdir tut
    EOH
    not_if { ::File.exists?("/opt/tut") }
end

remote_file '/opt/tut/moveus-cec-server.war' do
    source 'http://ftp.specs-project.eu/public/artifacts/all-components/moveus-cec-server.war'
    owner 'root'
    group 'root'
    mode '0755'
    action :create
    only_if { ! ::File.exists?("/opt/tut/moveus-cec-server.war") }
end

bash "execute_cec_server" do
    user "root"
    cwd  "/opt/tut"
    code <<-EOH
    java -jar moveus-cec-server.war > moveus_cec_server.log &
    EOH
end


