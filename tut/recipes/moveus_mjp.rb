#
# Cookbook Name:: sla-platform
# Recipe:: sla-manager
#
# Copyright 2010-2015, Cerict, Napoli
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#     ----------------------------------------
#
# This product includes software developed at "Company", http://url/ .
#
# Developers:
#  * De Rosa Pasquale <pakygta@gmail.com>
#

# get implementation plan
plan_id = node['implementation_plan_id']
plan = search("implementation_plans","id:#{plan_id}").first

if plan == nil then
    puts "Implementation Plan with id: #{plan_id} is not present in Implementation plans data bag"
    exit
end

# get cec server ip

cec_server_ip="127.0.0.1"

plan['csps'].each do | csp |
	csp['pools'].each do | pool |
    		pool['vms'].each do | vm |
        		vm['components'].each do | component |
            
            	if (component['cookbook']=="tut" and component['recipe']=="moveus_cec_server") then
                cec_server_ip=vm['public_ip']
                if(cec_server_ip==nil) then
                    cec_server_ip=component['private_ips'][0]
                end
            end
            
        end
    end
end
end


include_recipe "enabling-platform::jdk_v8"

bash "folder_creation" do
    user "root"
    cwd  "/opt"
    code <<-EOH
    mkdir tut
    EOH
    not_if { ::File.exists?("/opt/tut") }
end

remote_file '/opt/tut/moveus-mjp.war' do
    source 'http://ftp.specs-project.eu/public/artifacts/all-components/moveus-mjp.war'
    owner 'root'
    group 'root'
    mode '0755'
    action :create
    only_if { ! ::File.exists?("/opt/tut/moveus-mjp.war") }
end

bash "execute_tut_server" do
    user "root"
    cwd  "/opt/tut"
    code <<-EOH
    java -jar moveus-mjp.war --cec-endpoint="http://#{cec_server_ip}:9090" > moveus_mjp.log &
    EOH
end


